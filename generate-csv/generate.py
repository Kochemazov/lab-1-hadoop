import pandas as pd
import numpy as np
from datetime import datetime, date, time
import random
counter = (range(1,2000))
list_data = []
nodes_data = []
values_data = []
for i in counter:
    
    month = random.randint(1, 1)
    day = random.randint(1, 1)
    hour = random.randint(0, 23)
    minute = random.randint(0, 59)
    second = random.randint(0, 59)
    values = random.randint(0,200)
    nodes = random.randint(1,6)
    start_date = datetime(2022, month, day, hour,minute, second)
    c = int((datetime.timestamp(start_date))) * 1000
    list_data.append(c)
    values_data.append(values)
    nodes_data.append(nodes)
#     nodes_data.append(random.choice(('Node1CPU', 'Node2RAMmb', 'Node2CPU','Node2RAMmb')))
ser_list_data = pd.Series(list_data)
ser_nodes_data = pd.Series(nodes_data)
ser_values_data = pd.Series(values_data)
frame = { 'nodes': ser_nodes_data, 'milies': ser_list_data,'values': ser_values_data}
df = pd.DataFrame(frame)
df = df.sort_values(by='milies', ascending=False)
df = df.reset_index()
del df['index']
df.set_index('nodes', inplace=True)
df.to_csv('done_out.csv') 