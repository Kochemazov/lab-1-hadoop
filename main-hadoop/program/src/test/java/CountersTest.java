
import bdtc.lab1.CounterType;
import bdtc.lab1.HW1Mapper;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;


public class CountersTest {

    private MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;

    private final String testMetric = "1,1616188260099,10";
    private final String ExpectedMapperResult = "Node1CPU, 1616188260000, 60s";
    private final String testMalformedMetric = "mama mila ramu";
    private final int timeRangeForMetrics = 60;


    @Before
    public void setUp() {
        HW1Mapper mapper = new HW1Mapper();
        mapper.setTimeRangeForMetrics(timeRangeForMetrics);
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void testMapperCounterOne() throws IOException {
        mapDriver
                .withInput(new LongWritable(), new Text(testMalformedMetric))
                .runTest();
        assertEquals("Expected 1 counter increment", 1, mapDriver.getCounters()
                .findCounter(CounterType.MALFORMED).getValue());
    }

    @Test
    public void testMapperCounterZero() throws IOException {

        mapDriver
                .withInput(new LongWritable(), new Text(testMetric))
                .withOutput(new Text(ExpectedMapperResult), new IntWritable(10))
                .runTest();
        assertEquals("Expected 1 counter increment", 0, mapDriver.getCounters()
                .findCounter(CounterType.MALFORMED).getValue());
    }

    @Test
    public void testMapperCounters() throws IOException {
        mapDriver
                .withInput(new LongWritable(), new Text(testMetric))
                .withInput(new LongWritable(), new Text(testMalformedMetric))
                .withInput(new LongWritable(), new Text(testMalformedMetric))
                .withOutput(new Text(ExpectedMapperResult), new IntWritable(10))
                .runTest();

        assertEquals("Expected 2 counter increment", 2, mapDriver.getCounters()
                .findCounter(CounterType.MALFORMED).getValue());
    }
}

