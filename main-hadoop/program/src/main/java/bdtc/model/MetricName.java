package bdtc.model;

/**
 * enum представляет собой справочник соответствия id метрики с ее названием
 */
public enum MetricName {

    N1(1, "Node1CPU"),
    N2(2, "Node2CPU"),
    N3(3, "Node3CPU"),
    N4(4, "Node4CPU"),
    N5(5, "Node5CPU"),
    N6(6, "Node1Memory"),
    N7(7, "Node2Memory"),
    N8(8, "Node3Memory"),
    N9(9, "Node4Memory"),
    UNDEFINED(999999, "Undefined");

    public final int id;
    public final String name;

    private MetricName(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Метод позволяет получить MetricName по id метрики
     * @param id метрики
     * @return MetricName
     */
    public static MetricName retrieveById(int id) {
        for (MetricName metricName : MetricName.values()) {
            if (metricName.id == id)
                return metricName;
        }
        return UNDEFINED;
    }

}
