package bdtc.lab1;

import bdtc.model.Metric;
import bdtc.model.MetricParser;
import bdtc.model.ParseException;
import lombok.Setter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

@Setter
public class HW1Mapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private Integer timeRangeForMetrics;
    private Text word = new Text();

    /**
     * Mapper: ключ (metricName, timestamp, scale), значение (value)
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        Configuration configuration = context.getConfiguration();

        //данное условие необходимо для юнит тестов, так как там timeRangeForMetrics сеттиться напрямую
        if (timeRangeForMetrics == null)
            timeRangeForMetrics = Integer.parseInt(configuration.get("timeRangeForMetrics"));

        String line = value.toString();
        Metric metric;
        try {
            metric = MetricParser.parse(line);
        } catch (ParseException e) {
            context.getCounter(CounterType.MALFORMED).increment(1);
            return;
        }

        // вычисление начального времени агрегирования в секундах
        long startTime = metric.getMetricTimestamp() - metric.getMetricTimestamp() % (this.timeRangeForMetrics * 1000);

        // ключ состоит из:
        // -названия метрики
        // -начального времени агрегирования
        // -диапазон времени агрегации сырых метрик в секундах
        String wordString = String.join(", ", metric.getName().name, String.valueOf(startTime), this.timeRangeForMetrics + "s");
        word.set(wordString);
        context.write(word, new IntWritable(metric.getMetricValue()));

    }
}
